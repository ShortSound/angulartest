'use strict';

var myApp = angular.module('myApp', ['ngRoute'])
    .config(['$routeProvider', '$locationProvider', function ($routeProvide, $locationProvider) {
        $locationProvider.html5Mode(
            {
                enabled:true,
                requireBase: false
            }
        );
        $routeProvide.when('/person/:personid', {
            templateUrl: 'templates/person-info.html',
            controller: 'PersonController'
        }).when('/', {
            templateUrl: 'templates/home.html',
            controller: 'MyAppController'
        }).otherwise({
            redirectTo: '/'
        });
    }]);

myApp.controller('PersonController', ['$scope', '$http', '$location', '$routeParams', 'personFactory', function ($scope, $http, $location, $routeParams, personFactory) {
    $scope.personid = $routeParams.personid;
    $scope.newPersons = personFactory;

}]);
myApp.controller('MyAppController', ["$scope", "personFactory", function ($scope, personFactory) {
    console.log(personFactory);
    $scope.person = personFactory;
    $scope.editPerson = function (person) {
        return $scope.isEditing[person] = !$scope.isEditing[person];

    };
    $scope.removeTag = function (person) {
        for (var i in personFactory) if (personFactory[i] === person)
            personFactory.splice(i, 1);
    };

    $scope.addTag = function () {
        personFactory.push({name: $scope.personName, number: $scope.personNumber, address: $scope.personAddress, biography: ""});
    };
    $scope.isEditing = [];
    // $scope.isEditingName = [];
    // $scope.isEditingNumber = [];
    // $scope.isEditingAddress = [];
    // console.log($scope.isEditing);


}]);


myApp.factory('personFactory', function() {
    return [
        {
            name: 'Kate',
            number: 1234,
            address: "some street",
            biography: "some biography"
        }, {
            name: 'Keil',
            number: 3214,
            address: "some street",
            biography: "some biography"
        }, {
            name: 'Adamus',
            number: 2345,
            address: "some street",
            biography: "some biography"
        }, {
            name: 'Sadam',
            number: 6345,
            address: "some street",
            biography: "some biography"
        }
    ];
});
